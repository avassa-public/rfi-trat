FROM python:3-alpine

ARG VERSION

WORKDIR /app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY canbus.py .

ENV PYTHONUNBUFFERED=1
ENV VERSION=${VERSION}

CMD [ "python", "./canbus.py" ]
