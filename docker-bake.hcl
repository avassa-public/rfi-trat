variable "CI_REGISTRY_IMAGE" {
    default = "test"
}

variable "CI_COMMIT_REF_SLUG" {
    default = "test"
}

target "canbus" {
    platforms = ["linux/amd64"]
    cache-from = ["type=registry,ref=${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-cache"]
    cache-to = ["type=registry,ref=${CI_REGISTRY_IMAGE}:${CI_COMMIT_REF_SLUG}-cache"]
    dockerfile = "Dockerfile"
    context = "."
    name="canbus-${replace(version, ".", "-")}"
    tags = [
        "${CI_REGISTRY_IMAGE}/${app}",
        "${CI_REGISTRY_IMAGE}/${app}:${version}",
    ]
    args = {
      VERSION = "${version}"
    }
    matrix = {
      app = [
        "canbus"
      ]
      version = [
         "v1.0",
         "v2.0"
      ]
    }
    output = ["type=registry"]
}
