import json
import os
import random
import threading
import time

from fastapi import FastAPI, HTTPException
from contextlib import asynccontextmanager
import paho.mqtt.client as mqtt
import ssl

version=os.environ.get("VERSION", default="Unknown")

alive = threading.Event()
alive.set()
acceleration=0.0
velocity=0.0

def data_simulator():
    global velocity
    global acceleration

    print("Starting simulator")
    
    delta_t = 0.5
    while alive.is_set():
        acceleration=random.random() - 0.25
        velocity+=acceleration*delta_t
        if velocity > 25.0:
            velocity = 25
        if velocity < -2.8:
            velocity = -2.8
        time.sleep(delta_t)


def mqtt_on_connect(_client, _userdata, _flags, _reason_code, _properties):
    print("MQTT connected")


def mqtt_reporter():
    global velocity
    global acceleration
    mqtt_host = os.environ.get("MQTT_HOST", None)
    if mqtt_host is None:
        print("No MQTT configuration")
        return

    mqttc = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
    mqttc.on_connect = mqtt_on_connect
    mqttc.connect(mqtt_host, 8883, 60)
    mqttc.username_pw_set(os.environ["MQTT_USERNAME"],os.environ['MQTT_PASSWORD'])
    mqttc.tls_set(certfile=None, keyfile=None, cert_reqs=ssl.CERT_REQUIRED)
    mqttc.loop_start()

    while alive.is_set():
        payload = {
                "vehicle": os.environ["VEHICLE"],
                "velocity": velocity,
                "acceleration": acceleration,
                }
        mqttc.publish("/vehicle", payload=json.dumps(payload))
        time.sleep(10)


def printer():
    while alive.is_set():
        print(f"Velocity: {velocity}")
        time.sleep(1)

description="""
CANBUS
"""

@asynccontextmanager
async def lifespan(app: FastAPI):
    ths=[
            threading.Thread(target=data_simulator),
            threading.Thread(target=mqtt_reporter),
            threading.Thread(target=printer)
            ]
    for th in ths:
        th.start()

    yield
    alive.clear()
    for th in ths:
        th.join()


app = FastAPI(title="Avassa CANBUS", description=description, lifespan=lifespan)

@app.get("/ready")
def is_ready():
    not_ready = os.path.exists("/not_ready")
    if not_ready:
        raise HTTPException(status_code=400, detail=f"Not ready")
    return "Ready"

if __name__ == '__main__':
    print(f"Version: {version}")
    import uvicorn
    uvicorn.run(
        "canbus:app",
        host=os.environ.get("HOST", "0.0.0.0"),
        port=int(os.environ.get("PORT", 8080)),
        reload=True,
    )
